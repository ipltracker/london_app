echo "Stopping NGINX Server.." && sudo service nginx stop
dir="/var/www/testiplsmscom"
rm -rf $dir && echo "Removed Web Directory"
mkdir $dir && echo "Created Web Directory"
cd $dir && git clone --depth=1 https://iambharathk@bitbucket.org/ipltracker/london_app.git $dir && rm -rf .git bitbucket-pipelines.yml
cp .env.example .env 
sudo chown -R $(whoami):www-data $dir && echo "Applying Web Directory Permissions"
composer install 
php $dir/artisan config:clear && php $dir/artisan key:generate
sudo service nginx start && echo "Starting NGINX Server.." 
